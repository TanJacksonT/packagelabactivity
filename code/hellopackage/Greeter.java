package hellopackage;

import secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;
import java.util.*;

/** 
 * Greeting is a class used for testing packages out
 * information about rectangle
 * @author Dan Pomerantz 
 * @version 5/13/2020
 * */

public class Greeter {
    public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    System.out.println("Enter a number");
    int numberStored = sc.nextInt();
    Utilities newobj = new Utilities();
    System.out.println(newobj.doubleMe(numberStored));
    }

}