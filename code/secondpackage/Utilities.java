package secondpackage;

/**
 * Doubles the input value
 * 
 * @param x The value to be doubled
 * @return The value of x times two
 */

public class Utilities {
    public int doubleMe(int x) {
        return x*2;        
    }

}